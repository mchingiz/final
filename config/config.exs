# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :final,
  ecto_repos: [Final.Repo]

# Configures the endpoint
config :final, FinalWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "c8qA/zd3+k11F5RI8g+Ivuw2sEBo+MxIlvvB4s7jv30ePLngrYBgqEYq1x2680FN",
  render_errors: [view: FinalWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Final.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
