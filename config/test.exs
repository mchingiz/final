use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :final, FinalWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :final, Final.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "admin",
  database: "final_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# Add the following lines at the end of the file
config :hound, driver: "chrome_driver"
config :final, sql_sandbox: true
