defmodule FinalWeb.GameControllerTest do
  use FinalWeb.ConnCase

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Final.Repo
  alias Final.Models
  alias Final.Models.Game
  alias Final.Models.Frame


  # Requirement 1
  test "New game with rejection [username already used]", %{conn: conn} do
    old_game_count = Repo.one(from g in Game, select: fragment("count(*)"))

    conn = post conn, "/games", %{game: %{username: "chingiz"}}
    # conn = get conn, redirected_to(conn)

    new_game_count = Repo.one(from g in Game, select: fragment("count(*)"))

    assert old_game_count == new_game_count
  end

  # Requirement 1
  test "New game is created and user is redirected to frame 1", %{conn: conn} do
    old_game_count = Repo.one(from g in Game, select: fragment("count(*)"))

    conn = post conn, "/games", %{game: %{username: "cersei"}}
    conn = get conn, redirected_to(conn)

    new_game_count = Repo.one(from g in Game, select: fragment("count(*)"))

    assert old_game_count+1 == new_game_count
    assert html_response(conn, 200) =~ ~r/Frame 1/
  end
end
