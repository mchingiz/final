defmodule FinalWeb.FrameControllerTest do
  use FinalWeb.ConnCase

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Final.Repo
  alias Final.Models
  alias Final.Models.Frame
  alias Final.Models.Game

  # Requirement 3
  test "Roll 3 can't be more than 0 until frame 10", %{conn: conn} do
    conn = post conn, "/frames", %{frame: %{username: "chingiz", frameno: "1", r1: "4", r2: "4", r3: "1"}}

    frame = Repo.one(from f in Frame, order_by: [desc: f.id], limit: 1)

    r3 =  frame |> Map.from_struct() |> Map.fetch(:r3)

    assert elem(r3,1) == nil
  end

  # Requirement 3
  test "Roll 3 can be more than 0 at frame 10", %{conn: conn} do
    conn = post conn, "/frames", %{frame: %{username: "chingiz", frameno: "10", r1: "4", r2: "4", r3: "1"}}

    frame = Repo.one(from f in Frame, order_by: [desc: f.id], limit: 1)

    r3 =  frame |> Map.from_struct() |> Map.fetch(:r3)

    assert elem(r3,1) == 1
  end

  # Requirement 2
  test "Sum first two rolls can not be more than 10", %{conn: conn} do
    old_frame_count = Repo.one(from g in Frame, select: fragment("count(*)"))

    conn = post conn, "/frames", %{frame: %{username: "chingiz", frameno: "2", r1: "6", r2: "6", r3: "0"}}
    conn = get conn, redirected_to(conn)

    new_frame_count = Repo.one(from g in Frame, select: fragment("count(*)"))

    assert html_response(conn, 200) =~ ~r/Sum of first two rolls cannot be greater than 10/
    assert old_frame_count == new_frame_count
  end

  # Requirement 4
  test "Final score is shown after submitting the last frame", %{conn: conn} do
    conn = post conn, "/frames", %{frame: %{username: "ilyas", frameno: "10", r1: "4", r2: "4", r3: "1"}}
    conn = get conn, redirected_to(conn)

    assert html_response(conn, 200) =~ ~r/Your final score is/
    assert html_response(conn, 200) =~ ~r/74/
  end
end
