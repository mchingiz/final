defmodule Final.ModelsTest do
  use Final.DataCase

  alias Final.Models

  # describe "games" do
  #   alias Final.Models.Game

  #   @valid_attrs %{username: "some username"}
  #   @update_attrs %{username: "some updated username"}
  #   @invalid_attrs %{username: nil}

  #   def game_fixture(attrs \\ %{}) do
  #     {:ok, game} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Models.create_game()

  #     game
  #   end

  #   test "list_games/0 returns all games" do
  #     game = game_fixture()
  #     assert Models.list_games() == [game]
  #   end

  #   test "get_game!/1 returns the game with given id" do
  #     game = game_fixture()
  #     assert Models.get_game!(game.id) == game
  #   end

  #   test "create_game/1 with valid data creates a game" do
  #     assert {:ok, %Game{} = game} = Models.create_game(@valid_attrs)
  #     assert game.username == "some username"
  #   end

  #   test "create_game/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Models.create_game(@invalid_attrs)
  #   end

  #   test "update_game/2 with valid data updates the game" do
  #     game = game_fixture()
  #     assert {:ok, game} = Models.update_game(game, @update_attrs)
  #     assert %Game{} = game
  #     assert game.username == "some updated username"
  #   end

  #   test "update_game/2 with invalid data returns error changeset" do
  #     game = game_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Models.update_game(game, @invalid_attrs)
  #     assert game == Models.get_game!(game.id)
  #   end

  #   test "delete_game/1 deletes the game" do
  #     game = game_fixture()
  #     assert {:ok, %Game{}} = Models.delete_game(game)
  #     assert_raise Ecto.NoResultsError, fn -> Models.get_game!(game.id) end
  #   end

  #   test "change_game/1 returns a game changeset" do
  #     game = game_fixture()
  #     assert %Ecto.Changeset{} = Models.change_game(game)
  #   end
  # end

  # describe "frames" do
  #   alias Final.Models.Frame

  #   @valid_attrs %{frameno: 42, r1: 42, r2: 42, r3: 42, username: "some username"}
  #   @update_attrs %{frameno: 43, r1: 43, r2: 43, r3: 43, username: "some updated username"}
  #   @invalid_attrs %{frameno: nil, r1: nil, r2: nil, r3: nil, username: nil}

  #   def frame_fixture(attrs \\ %{}) do
  #     {:ok, frame} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Models.create_frame()

  #     frame
  #   end

  #   test "list_frames/0 returns all frames" do
  #     frame = frame_fixture()
  #     assert Models.list_frames() == [frame]
  #   end

  #   test "get_frame!/1 returns the frame with given id" do
  #     frame = frame_fixture()
  #     assert Models.get_frame!(frame.id) == frame
  #   end

  #   test "create_frame/1 with valid data creates a frame" do
  #     assert {:ok, %Frame{} = frame} = Models.create_frame(@valid_attrs)
  #     assert frame.frameno == 42
  #     assert frame.r1 == 42
  #     assert frame.r2 == 42
  #     assert frame.r3 == 42
  #     assert frame.username == "some username"
  #   end

  #   test "create_frame/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Models.create_frame(@invalid_attrs)
  #   end

  #   test "update_frame/2 with valid data updates the frame" do
  #     frame = frame_fixture()
  #     assert {:ok, frame} = Models.update_frame(frame, @update_attrs)
  #     assert %Frame{} = frame
  #     assert frame.frameno == 43
  #     assert frame.r1 == 43
  #     assert frame.r2 == 43
  #     assert frame.r3 == 43
  #     assert frame.username == "some updated username"
  #   end

  #   test "update_frame/2 with invalid data returns error changeset" do
  #     frame = frame_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Models.update_frame(frame, @invalid_attrs)
  #     assert frame == Models.get_frame!(frame.id)
  #   end

  #   test "delete_frame/1 deletes the frame" do
  #     frame = frame_fixture()
  #     assert {:ok, %Frame{}} = Models.delete_frame(frame)
  #     assert_raise Ecto.NoResultsError, fn -> Models.get_frame!(frame.id) end
  #   end

  #   test "change_frame/1 returns a frame changeset" do
  #     frame = frame_fixture()
  #     assert %Ecto.Changeset{} = Models.change_frame(frame)
  #   end
  # end
end
