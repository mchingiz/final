# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Final.Repo.insert!(%Final.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Final.{Repo, Models.Game, Models.Frame}

Ecto.Adapters.SQL.query!(Repo, "TRUNCATE TABLE games RESTART IDENTITY CASCADE", [])
Ecto.Adapters.SQL.query!(Repo, "TRUNCATE TABLE frames RESTART IDENTITY CASCADE", [])

# Products

[%{username: "chingiz"}, # 1
 %{username: "gunel"}, # 2
 %{username: "ilyas"}, # 3
 %{username: "tofig"}] # 4
|> Enum.map(fn game_data -> Game.changeset(%Game{}, game_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

# Frames
# 76
[%{username: "chingiz", r1: 1, r2: 4, r3: nil, frameno: 1}, # 5
 %{username: "chingiz", r1: 0, r2: 4, r3: nil, frameno: 2}, # 4
 %{username: "chingiz", r1: 0, r2: 5, r3: nil, frameno: 3}, # 5
 %{username: "chingiz", r1: 5, r2: 0, r3: nil, frameno: 4}, # 5
 %{username: "chingiz", r1: 10, r2: 0, r3: nil, frameno: 5}, # 17
 %{username: "chingiz", r1: 5, r2: 2, r3: nil, frameno: 6}, # 7
 %{username: "chingiz", r1: 5, r2: 5, r3: nil, frameno: 7}, # 13
 %{username: "chingiz", r1: 3, r2: 4, r3: nil, frameno: 8}, # 7
 %{username: "chingiz", r1: 4, r2: 3, r3: nil, frameno: 9}, # 7
 %{username: "chingiz", r1: 3, r2: 2, r3: 1, frameno: 10}, # 6
 %{username: "ilyas", r1: 1, r2: 4, r3: nil, frameno: 1}, # 5
 %{username: "ilyas", r1: 0, r2: 4, r3: nil, frameno: 2}, # 4
 %{username: "ilyas", r1: 0, r2: 5, r3: nil, frameno: 3}, # 5
 %{username: "ilyas", r1: 5, r2: 0, r3: nil, frameno: 4}, # 5
 %{username: "ilyas", r1: 10, r2: 0, r3: nil, frameno: 5}, # 17
 %{username: "ilyas", r1: 5, r2: 2, r3: nil, frameno: 6}, # 7
 %{username: "ilyas", r1: 5, r2: 5, r3: nil, frameno: 7}, # 13
 %{username: "ilyas", r1: 3, r2: 4, r3: nil, frameno: 8}, # 7
 %{username: "ilyas", r1: 4, r2: 3, r3: nil, frameno: 9}, # 7
 %{username: "tofig", r1: 3, r2: 3, r3: 0, frameno: 1}]
|> Enum.map(fn frame_data -> Frame.changeset(%Frame{}, frame_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
