defmodule FinalWeb.GameController do
  use FinalWeb, :controller

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Final.Repo
  alias Final.Models
  alias Final.Models.Game
  alias Final.Models.Frame

  def index(conn, _params) do
    games = Models.list_games()
    render(conn, "index.html", games: games)
  end

  def new(conn, _params) do
    changeset = Models.change_game(%Game{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"game" => game_params}) do
    username = game_params["username"]
    g = Repo.get_by(Game, username: username)

    if g != nil do
      conn
      |> put_flash(:error, "This username has already been used")
      |> redirect(to: game_path(conn, :new))
    else
      # text conn, "succeess with username "<>username

      case Models.create_game(game_params) do
        {:ok, game} ->
          conn
          |> put_flash(:info, "Game created successfully.")
          |> redirect(to: frame_path(conn, :new, username: username, frameno: 1))
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "new.html", changeset: changeset)
      end

    end


  end

  def show(conn, %{"id" => id}) do
    game = Models.get_game!(id)
    render(conn, "show.html", game: game)
  end

  def edit(conn, %{"id" => id}) do
    game = Models.get_game!(id)
    changeset = Models.change_game(game)
    render(conn, "edit.html", game: game, changeset: changeset)
  end

  def update(conn, %{"id" => id, "game" => game_params}) do
    game = Models.get_game!(id)

    case Models.update_game(game, game_params) do
      {:ok, game} ->
        conn
        |> put_flash(:info, "Game updated successfully.")
        |> redirect(to: game_path(conn, :show, game))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", game: game, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    game = Models.get_game!(id)
    {:ok, _game} = Models.delete_game(game)

    conn
    |> put_flash(:info, "Game deleted successfully.")
    |> redirect(to: game_path(conn, :index))
  end
end
