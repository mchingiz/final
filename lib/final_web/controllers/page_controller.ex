defmodule FinalWeb.PageController do
  use FinalWeb, :controller

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Final.Repo
  alias Final.Models
  alias Final.Models.Frame
  alias Final.Models.Game

  def index(conn, _params) do
    conn |> redirect(to: game_path(conn, :new))
    # render conn, "index.html"
  end

  def gameover(conn, %{"username" => username}) do
    # text conn, "end of game for"<>username

    query = (from f in Frame, select: [f.r1, f.r2, f.r3], where: f.username == ^username)
    rolls = Repo.all(query)

    finalscore = Game.calculate(rolls)

    render(conn, "gameover.html", finalscore: finalscore)
  end

  def test(conn, _params) do
    count = Repo.one(from g in Game, select: fragment("count(*)"))
    text conn, "total count"<>Integer.to_string(count)

    # conn |> redirect(to: frame_path(conn, :new, username: "chingiz", frameno: 3))

    # render conn, "index.html"
  end
end
