defmodule FinalWeb.FrameController do
  use FinalWeb, :controller

  import Ecto.Query, only: [from: 2]
  alias Ecto.{Changeset, Multi}
  alias Final.Repo
  alias Final.Models
  alias Final.Models.Frame
  alias Final.Models.Game

  def index(conn, _params) do
    frames = Models.list_frames()
    render(conn, "index.html", frames: frames)
  end

  def new(conn, %{"username" => username, "frameno" => frameno}) do
    changeset = Models.change_frame(%Frame{})
    render(conn, "new.html", changeset: changeset, username: username, frameno: frameno)
  end

  def create(conn, %{"frame" => frame_params}) do
    # text conn, "succeess with username "<>username

    u = frame_params["username"]
    fno = frame_params["frameno"]
    next_fno = elem(Integer.parse(fno),0)+1
    r1 = frame_params["r1"]
    r2 = frame_params["r2"]
    r3 = frame_params["r3"]

    frame_params =  if fno != "10" do
                      %{"frameno" => fno, "r1" => r1, "r2" => r2, "r3" => nil, "username" => u}
                    else
                      frame_params
                    end

    if elem(Integer.parse(r1),0)+elem(Integer.parse(r2),0) > 10 do
      conn
      |> put_flash(:error, "Sum of first two rolls cannot be greater than 10")
      |> redirect(to: frame_path(conn, :new, username: u, frameno: elem(Integer.parse(fno),0)))
    else
      case Models.create_frame(frame_params) do
        {:ok, frame} ->
          if fno == "10" do
            # text conn, "end of game"
            g = Repo.get_by(Game, username: u)

            conn
            |> put_flash(:info, "Game has ended. Here is your results")
            |> redirect(to: page_path(conn, :gameover, u))
          else
            conn
            |> put_flash(:info, "Frame #{fno} saved!")
            |> redirect(to: frame_path(conn, :new, username: u, frameno: next_fno))
          end
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "new.html", changeset: changeset)
      end
    end
  end

  def show(conn, %{"id" => id}) do
    frame = Models.get_frame!(id)
    render(conn, "show.html", frame: frame)
  end

  def edit(conn, %{"id" => id}) do
    frame = Models.get_frame!(id)
    changeset = Models.change_frame(frame)
    render(conn, "edit.html", frame: frame, changeset: changeset)
  end

  def update(conn, %{"id" => id, "frame" => frame_params}) do
    frame = Models.get_frame!(id)

    case Models.update_frame(frame, frame_params) do
      {:ok, frame} ->
        conn
        |> put_flash(:info, "Frame updated successfully.")
        |> redirect(to: frame_path(conn, :show, frame))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", frame: frame, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    frame = Models.get_frame!(id)
    {:ok, _frame} = Models.delete_frame(frame)

    conn
    |> put_flash(:info, "Frame deleted successfully.")
    |> redirect(to: frame_path(conn, :index))
  end
end
