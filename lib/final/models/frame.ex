defmodule Final.Models.Frame do
  use Ecto.Schema
  import Ecto.Changeset


  schema "frames" do
    field :frameno, :integer
    field :r1, :integer
    field :r2, :integer
    field :r3, :integer
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(frame, attrs) do
    frame
    |> cast(attrs, [:username, :r1, :r2, :r3, :frameno])
    |> validate_required([:username, :r1, :r2, :frameno])
    |> validate_number(:r1, greater_than_or_equal_to: 0, less_than_or_equal_to: 10)
    |> validate_number(:r2, greater_than_or_equal_to: 0, less_than_or_equal_to: 10)
    |> validate_number(:r3, greater_than_or_equal_to: 0, less_than_or_equal_to: 10)
  end
end
