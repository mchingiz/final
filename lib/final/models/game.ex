defmodule Final.Models.Game do
  use Ecto.Schema
  import Ecto.Changeset


  schema "games" do
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:username])
    |> validate_required([:username])
  end

  def calculate(rolls) do
    list =  List.flatten(rolls)
            |> Enum.filter(fn e -> is_number(e) end)    # &is_number/1

    count_points(list)
  end

  def count_points([]), do: 0
  def count_points([h1, h2, h3]) when h1 == 10 or h1 + h2 == 10, do: h1 + h2 + h3
  def count_points([10 | [h2 | tail]]), do: 10 + h2 + hd(tail) + count_points([h2 | tail])
  def count_points([h1 | [h2 | tail]]) when h1 + h2 == 10, do: 10 + hd(tail) + count_points(tail)
  def count_points([h1 | [h2 | tail]]), do: h1 + h2 + count_points(tail)
end
