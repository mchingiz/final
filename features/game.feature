Feature: starting a game
  As a User
  Such that has already entered data of first 9 frames
  I want to enter data of last frame and see the result

  Scenario: enter data with success
    Given following frame list
        | username | frameno | r1 | r2 | r3  |
        | chingiz2 | 1       | 1  | 4  | 0   |
        | chingiz2 | 2       | 0  | 4  | 0   |
        | chingiz2 | 3       | 0  | 5  | 0   |
        | chingiz2 | 4       | 5  | 0  | 0   |
        | chingiz2 | 5       | 10 | 0  | 0   |
        | chingiz2 | 6       | 5  | 2  | 0   |
        | chingiz2 | 7       | 5  | 5  | 0   |
        | chingiz2 | 8       | 3  | 4  | 0   |
        | chingiz2 | 9       | 4  | 3  | 0   |
    And I open last frame page
    And I enter "3","2","1" as rolls
    When I submit the form
    Then I should see "71" as my final result