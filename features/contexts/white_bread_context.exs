defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Final.{Repo, Models.Game, Models.Frame}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Final.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Final.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Final.Repo)
    # Hound.end_session
    nil
  end

  given_ ~r/^following frame list$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn frame -> Frame.changeset(%Frame{}, frame) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I open last frame page$/, fn state ->
    navigate_to "/frames/new?username=chingiz2&frameno=10"
    {:ok, state}
  end

  and_ ~r/^I enter "(?<r1>[^"]+)","(?<r2>[^"]+)","(?<r3>[^"]+)" as rolls$/,
    fn state, %{r1: r1,r2: r2,r3: r3} ->
      fill_field({:id, "r1"}, r1)
      fill_field({:id, "r2"}, r2)
      fill_field({:id, "r3"}, r3)
      {:ok, state}
  end

  when_ ~r/^I submit the form$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should see "(?<finalpoint>[^"]+)" as my final result$/,
    fn state, %{finalpoint: finalpoint} ->
      assert visible_in_page? ~r/71/
      {:ok, state}
  end

  # given_ ~r/^following game list$/, fn state, %{table_data: table} ->
  #   table
  #   |> Enum.map(fn game -> Game.changeset(%Game{}, game) end)
  #   |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
  #   {:ok, state}
  # end

  # when_ ~r/^I open new game page$/, fn state ->
  #   navigate_to "/"
  #   {:ok, state}
  # end

  # and_ ~r/^I enter "(?<username>[^"]+)" as username$/,
  #   fn state, %{username: username} ->
  #     fill_field({:id, "username"}, username)
  #     {:ok, state}
  # end

  # when_ ~r/^I submit the form$/, fn state ->
  #   click({:id, "submit_button"})
  #   {:ok, state}
  # end

  # then_ ~r/^I should receive a confirmation message$/, fn state ->
  #   assert visible_in_page? ~r/Frame 1/
  #   {:ok, state}
  # end

  # then_ ~r/^I should receive a rejection message$/, fn state ->
  #   assert visible_in_page? ~r/This username has already been used/
  #   {:ok, state}
  # end
end
