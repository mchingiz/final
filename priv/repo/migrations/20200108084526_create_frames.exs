defmodule Final.Repo.Migrations.CreateFrames do
  use Ecto.Migration

  def change do
    create table(:frames) do
      add :username, :string
      add :r1, :integer
      add :r2, :integer
      add :r3, :integer
      add :frameno, :integer

      timestamps()
    end

  end
end
